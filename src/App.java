import java.util.ArrayList;
import java.util.Collections;

import java.util.Random;

public class App {
    public static void longArrayToArrayList() {
        Random random = new Random();
        long[] longArray = new long[100];
        // Create list number
        for (int i = 0; i < 100; i++) {
            longArray[i] = Long.valueOf(random.nextLong());

        }
        // for (int i = 0; i < longArray.length; i++) {
        // System.out.println(longArray[i]);
        // }
        // Define a new int array with the same length of your result array
        int[] intNumbers = new int[longArray.length];

        // Loop through all the result numbers
        for (int i = 0; i < longArray.length; i++) {
            // Cast to int and put it to the int array
            intNumbers[i] = (int) longArray[i];
        }
        // for (int i = 0; i < intNumbers.length; i++) {
        // System.out.println(intNumbers[i]);
        // }

        Integer[] arr = new Integer[intNumbers.length];
        // Loop through all the result numbers
        for (int i = 0; i < intNumbers.length; i++) {
            // Cast to int and put it to the int array
            arr[i] = (Integer) intNumbers[i];
        }
        ArrayList<Integer> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, arr);
        System.out.println(arrayList);

        // ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(intArray));
        // System.out.println(arrayList);

        // for (int i = 0; i < intArray.length; i++) {

        // }

    }

    public static void doubleArrayToArrayList() {
        Random random = new Random();
        double[] doubleArray = new double[100];
        // Create list number
        for (int i = 0; i < 100; i++) {
            doubleArray[i] = Double.valueOf(random.nextDouble());

        }

        // for (int i = 0; i < intNumbers.length; i++) {
        // System.out.println(intNumbers[i]);
        // }

        Double[] arr = new Double[doubleArray.length];
        // Loop through all the result numbers
        for (int i = 0; i < doubleArray.length; i++) {
            // Cast to int and put it to the int array
            arr[i] = (Double) doubleArray[i];
        }
        ArrayList<Double> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, arr);
        System.out.println(arrayList);

        // ArrayList<Integer> arrayList = new ArrayList<>(Arrays.asList(intArray));
        // System.out.println(arrayList);

        // for (int i = 0; i < intArray.length; i++) {

        // }

    }

    public static void main(String[] args) throws Exception {

        App.longArrayToArrayList();
        App.doubleArrayToArrayList();

    }
}
